To test/implement:

- create and test XSS vuln (not applicable?)
- Fake json token with alg: 'none' ignores signature?
- Timing attack on HMAC
- User specific routes middleware? ['sales', 'operations'] in route?
- ...
