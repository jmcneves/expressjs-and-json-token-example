//<Example>
// Inside this example tag are fake things that should be replaced by real ones
// Just skip this part

var database = [
  {username: 'Potato', email: 'potato@gmail.com', hash: 'potatoeh', 
  groups: ['sales']},

  {username: 'Agent Cucumber', email: 'cucumber@fbi.com', hash: 'cucumbereh', 
  groups: ['fbi']},

  {username: 'Admin', email: 'admin@fbi.com', hash: 'admineh', 
  groups: ['fbi', 'admin']}
];
// Secret key to sign the json tokens
var SECRET_KEY = 'someSecretKey';
// List of all logged out (and therefore invalid) tokens (Redis)
// We can delete them as the tokens expire because they really become invalid
var token_blacklist = [];
// Get user from the database (during login)
function getUser(email){
  for(var i = 0; i < database.length; i++){
    if(database[i].email === email){return database[i];}
  }
  return null;
}
function generateHash(password){
  return password + 'eh'; // Salted hash o.O
}
// Variable to be changed by CSRF
var state = false;
//</Example>


//-----------------------------------------------------------------------------
const expressjwt   = require('express-jwt');
const jwt          = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const express      = require('express');
const moment       = require('moment');
const uuid         = require('node-uuid');
const helmet       = require('helmet');

const PORT         = 3000;

var app = express();
var MODE = app.get('env');
app.use(helmet.hidePoweredBy({ setTo: 'hamsters' }));
app.use(cookieParser());
app.use(bodyParser.json());
//app.use(helmet());
app.use(helmet.contentSecurityPolicy({
  // Specify directives as normal.
  directives: {
    defaultSrc: ["'none'"], // "'self'"
    scriptSrc: [],
    styleSrc: [],
    fontSrc: [],
    imgSrc: [],
    mediaSrc: [],
    frameSrc: [],
    connectSrc: ["'self'"],
//    sandbox: ['allow-scripts'], // 'allow-forms',
    reportUri: '/xxx',
    objectSrc: [] // An empty array allows nothing through
  },

  // Set to true if you only want browsers to report errors, not block them
  reportOnly: false,

  // Set to true if you want to blindly set all headers: Content-Security-Policy,
  // X-WebKit-CSP, and X-Content-Security-Policy.
  setAllHeaders: false,

  // Set to true if you want to disable CSP on Android where it can be buggy.
  disableAndroid: false,

  // Set to false if you want to completely disable any user-agent sniffing.
  // This may make the headers less compatible but it will be much faster.
  // This defaults to `true`.
  browserSniff: true
}));
app.use(helmet.dnsPrefetchControl({ allow: false }));
app.use(helmet.xssFilter());
app.use(helmet.frameguard({ action: 'deny' }));
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.noCache());
app.use(helmet.hsts({
  maxAge: 1000000000,
  includeSubdomains: true,
  force: true
  //, preload: true
}));


/**
 * Used as middleware to only allow some groups in a route.
 * Assumes the user is already authenticated and all valid issued json tokens 
 * have groups.
 */
function allowedGroups(groups){
  return function(req, res, next){
    var user_groups = req.user.groups;
    for(var i=0; i<user_groups.length; i++){
      if(groups.includes(user_groups[i])){
        next();
        return;
      }
    }
    next('Logged in user is not part of any of the allowed groups.');
  };
}

// Router for unauthenticated/public routes
var public_router = express.Router();
public_router.get('/', function (req, res) {
  res.send('Hello, welcome to my page.');
});
public_router.post('/xxx', function (req, res) {
  if (req.body) {
    console.log('CSP Violation: ', req.body);
  } else {
    console.log('CSP Violation: No data received!');
  }
  res.status(204).end()
});
public_router.get('/login', function (req, res) {
  res.send('Login form');
});
public_router.post('/login', function (req, res) {
  const { email, password } = req.body;
  var hash = generateHash(password);
  var user = getUser(email);

  if(user && hash === user.hash){
    var expiresIn = moment().add(5, 'days').toDate();
    var expires = 60*60*24;
    var token = jwt.sign(user, SECRET_KEY,{
      jwtid: uuid.v4(),
      expiresIn: expires //expiresIn
    });
    res.cookie('jwt_token', token, {
      httpOnly: true,
      //secure: true, // If in production
      expires: expiresIn
    }).json(user);
  }else{
    res.json({success: false, error: 'Failed to log in.'});
  }
});
app.use('/', public_router);


var authenticated_router = express.Router();
authenticated_router.use(expressjwt({
  secret: SECRET_KEY,
  getToken: function(req) {
    return req.cookies.jwt_token ? req.cookies.jwt_token : null;
  },
  isRevoked: function(req, payload, done){
    var tokenId = payload.jti;
    if(token_blacklist.includes(tokenId)){
      done('That token is no longer valid.', true);
    }else{
      done(null, false);
    }
  }
}).unless({path: [
  ///\/api\/public/i
]}));
authenticated_router.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('You\'re not allowed here.');
  }
});


// Router for api routes
var api_router = express.Router();
// Only available to authenticated users
api_router.get('/protected', function (req, res) {
  console.log(req.user);
  res.send('Only authenticated users can see this (you can see this).');
});
// Only available to authenticated users of groups 'fbi' or 'cia'.
api_router.get('/secret', allowedGroups(['fbi', 'cia']),
 function (req, res) {
  res.send('Welcome to the secret page, ' + req.user.username + '.\nOnly members of the groups cia or fbi can see this (you can see this).');
});
// Adds cookie to the black list until it expires
api_router.get('/logout', function (req, res) {
  console.log('User "' + req.user.username + '" logged out.');
  token_blacklist.push(req.cookies.jwt_token.jti);
  res.clearCookie('jwt_token');
  res.redirect('/');
});
// API routes start with /api
authenticated_router.use('/api', api_router);


// Router for user routes
var user_router = express.Router();
// Only available to authenticated users
user_router.get('/dashboard', function (req, res) {
  console.log(req.user);
  res.send('Welcome to the dashboard. Only authenticated users can see this  (you can see this).');
});
// User routes (like the dashboard etc)
authenticated_router.use('/', user_router);
app.use('/', authenticated_router); 

// Read vs write permissions? (get vs post)
//app.post('/protected', function (req, res){
//  var new_state = req.body.state;
//  res.send('State changed because you\'re authenticated.');
//});
//Allow /sales/* to 'sales' group only?
//etc
//then common group routes
//api_router.route('/user').use(allowedGroups(['user']))

app.listen(PORT, function () {
  console.log('App listening on port ' + PORT + '! Mode: ' + MODE);
});
